'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;
  //config = Module.loadConfig(),
var favicon = require('serve-favicon');

var ExpertadviceV1 = new Module('expertadvice-v1');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
ExpertadviceV1.register(function(app, auth, database) {
   app.set('views', __dirname +'/public/views');

  //We enable routing. By default the Package Object is passed to the routes
  ExpertadviceV1.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  ExpertadviceV1.menus.add({
    title: 'expertadviceV1 example page',
    link: 'expertadviceV1 example page',
    roles: ['authenticated'],
    menu: 'main'
  });
  
  // ExpertadviceV1.aggregateAsset('css', 'expertadviceV1.css');
ExpertadviceV1.aggregateAsset('css', 'app.min.1.css');
ExpertadviceV1.aggregateAsset('css', 'app.min.2.css');
//ExpertadviceV1.aggregateAsset('css', 'demo.css');
//ExpertadviceV1.aggregateAsset('vendors/bower_components/', 'animate.css');
//ExpertadviceV1.aggregateAsset('vendors/bower_components/', 'animate.min.css');




 ExpertadviceV1.angularDependencies(['mean-factory-interceptor']);

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    ExpertadviceV1.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    ExpertadviceV1.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    ExpertadviceV1.settings(function(err, settings) {
        //you now have the settings object
    });
    */
//if(config.favicon) {
  //  app.use(favicon(config.favicon));
  //} else {
  //  app.use(favicon(__dirname + '/expertadvice-v1/assets/img/favicon.ico'));
  //}

  // Adding robots and humans txt
 // app.useStatic(__dirname + '/expertadvice-v1/static');

 

  return ExpertadviceV1;
});
