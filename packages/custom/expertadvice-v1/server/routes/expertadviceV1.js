'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter

var mean = require('meanio');
module.exports = function(ExpertadviceV1, app, auth, database) {





  // Home route
  var index = require('../controllers/index')(ExpertadviceV1);
  app.route('/')
    .get(index.render);
  app.route('/assets/aggregatedassets')
    .get(index.aggregatedList);

  app.get('/*',function(req,res,next){
        res.header('workerID' , JSON.stringify(mean.options.workerid) );
        next(); // http://expressjs.com/guide.html#passing-route control
  });

  app.get('/assets/get-public-config', function(req, res){
    var config = mean.loadConfig();

    return res.send(config.public);
  });
};

/*
--> default code 
  app.get('/api/expertadviceV1/example/anyone', function(req, res, next) {
    res.send('Anyone can access this');
  });

  app.get('/api/expertadviceV1/example/auth', auth.requiresLogin, function(req, res, next) {
    res.send('Only authenticated users can access this');
  });

  app.get('/api/expertadviceV1/example/admin', auth.requiresAdmin, function(req, res, next) {
    res.send('Only users with Admin role can access this');
  });

  app.get('/api/expertadviceV1/example/render', function(req, res, next) {
    ExpertadviceV1.render('index', {
      package: 'expertadvice-v1'
    }, function(err, html) {
      //Rendering a view from the Package server/views
      res.send(html);
    });
  });
};
*/

/*

--> example from core 

'use strict';

var mean = require('meanio');

module.exports = function(System, app, auth, database) {

  // Home route
  var index = require('../controllers/index')(System);
  app.route('/')
    .get(index.render);
  app.route('/api/aggregatedassets')
    .get(index.aggregatedList);

  app.get('/*',function(req,res,next){
        res.header('workerID' , JSON.stringify(mean.options.workerid) );
        next(); // http://expressjs.com/guide.html#passing-route control
  });

  app.get('/api/get-public-config', function(req, res){
    var config = mean.loadConfig();

    return res.send(config.public);
  });
};
*/